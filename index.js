function Imobil(locatie, pret, dimensiune, poze = []) {
    this.locatie = locatie;
    this.pret = pret;
    this.dimensiune = dimensiune;
    this.poze = poze;

    this.addAgent = function (agent) {
        this.agent = agent;
    }

    this.addClientPot = function (clientPot, oferta) {
        this.clientPot = clientPot;
        this.oferta = oferta;
    }

    this.removeClientPot = function () {
        this.clientPot = null;
    }

    this.addProprietar = function (proprietar) {
        this.proprietar = proprietar;
    }

    this.metodaCalcul = function () {
        return (this.pret - this.oferta) * 100 / this.pret;
    }
}


function Poza(url, nr, descriere) {
    this.url = url;
    this.nr = nr;
    this.descriere = descriere;
}


function Apartament(locatie, pret, dimensiune, poze = [], etaj, nrCamere) {
    Imobil.call(this, locatie, pret, dimensiune, poze = []);
    this.etaj = etaj;
    this.nrCamere = nrCamere;
}


function Casa(locatie, pret, dimensiune, poze = [], nrCamere) {
    Imobil.call(this, locatie, pret, dimensiune, poze = []);
    this.nrCamere = nrCamere;
}


/*
function Teren(...args) {
    Imobil.apply(this, args);

}
*/


function Persoana(nume, varsta, email, telefon) {
    this.nume = nume;
    this.varsta = varsta;
    this.email = email;
    this.telefon = telefon;
}


function Client(nume, varsta, email, telefon, buget, locatiiInteres) {
    Persoana.call(this, nume, varsta, email, telefon);

    this.buget = buget;
    this.locatiiInteres = locatiiInteres;
}


/*
class Proprietar extends Persoana {
constructor(nume, varsta, email, telefon){
    super(nume, varsta, email, telefon);
    
}
}

class Agent extends Persoana {
constructor(nume, varsta, email, telefon){
    super(nume, varsta, email, telefon);
    
}
}
*/


//Instantiere imobile
let teren = new Imobil('Zona extravilana Oradea', 100, 250, new Poza('POZETEREN.com', 1, 'Teren nu asa mare'));
let casa = new Casa('Strada Silvaniei', 56000, 150, new Poza('pozecasa.ro', 2, "Recent construita"), 2);
let apartament = new Apartament("Strada Ronald Reagan", 35000, 100, new Poza("pozeapartament.com", 3, "Dimensiunea unei garsoniere"), 3, 1);
let vila = new Casa('Strada Sofiei', 350000, 350, new Poza('pozevile.ro', 4, "Vila spatioasa cu gradina mare"), 6);

//Instantiere propriertari
let prop1 = new Persoana('John Cena', 23, "john@yahoo.com", '0753358646');
let prop2 = new Persoana('Mihi', 35, 'mihi@yahoo.com', '0746945387');
let prop3 = new Persoana('Cata', 45, 'cata@yahoo.com', '0743998165');

//Adaugare proprietar
teren.addProprietar(prop1);
casa.addProprietar(prop3);
apartament.addProprietar(prop2);

// Instantiere agenti
let agent1 = new Persoana('Agent1', 55, 'agent1@yahoo.com', '074366849');
let agent2 = new Persoana('Agent2', 35, 'agent2@yahoo.com', '074366115');

//Adaugare agent
apartament.addAgent(agent1);
vila.addAgent(agent2);

//Instantiere clienti
let client1 = new Client('Client1', 22, 'client1@yahoo.com', '0748995376', 35000, ['Strada Ronald Reagan']);
let client2 = new Client('Client2', 25, 'client2@yahoo.com', '0748811376', 400000, ['Strada Sofiei', 'Strada Silvaniei']);

//Adaugare client potential
apartament.addClientPot(client1, 34000);
vila.addClientPot(client2, 300000);

//Creare lista imobile
let imobile = [teren, casa, apartament, vila];

//Afisare imobile far client potential
console.log('Imobile fara client potential: ')

imobile.forEach(imb => {
    if (imb.clientPot == null) {
        console.log(imb.locatie);
    }
});

console.log('---------------------------------')

//Afisare imobile cu procent de negociere mai mic de 10%
console.log('Imobile cu procent de negociere mai mic de 10%: ')

imobile.forEach(imb => {
    if (imb.clientPot) {
        if (imb.metodaCalcul() <= 10) {
            console.log(imb.locatie);
        }
    }
});